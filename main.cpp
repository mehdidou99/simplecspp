#include "ECS/ECS.hpp"
#include <SFML/Graphics.hpp>
#include <random>
#include <algorithm>

template<typename T>
struct Vec2D
{
    double x { 0 };
    double y { 0 };
};

using Position = ecs::Component<Vec2D<struct PositionTag>>;
using Velocity = ecs::Component<Vec2D<struct VelocityTag>>;

using struct RenderInfos
{
    sf::Shape* shape;
    sf::Color color;
};

using RenderComponent = ecs::Component<RenderInfos>;

class PhysicsSystem
{
public:
    void update(double dt, Position& p, Velocity& v);

    using Requirements = ecs::Requirements<ecs::ElapsedTime, Position, Velocity>;
    using UpdatePolicy = ecs::OncePerEntity;

private:
    double g { 9.81 };
}

class RenderingSystem
{
public:
    RenderingSystem(sf::RenderWindow& window);

    void update(RenderInfos const& render_infos, Position const& p);

    using Requirements = ecs::Requirements<RenderInfos, Position>;
    using UpdatePolicy = ecs::OncePerEntity;

private:
    sf::RenderWindow& m_window;
};

class PositionShufflingSystem
{
public:
    void update(ecs::ComponentRange const& positions);

    using Requirements = ecs::Requirements<ecs::ComponentRange<Position>>;
    using UpdatePolicy = ecs::OncePerUpdate;
}

int main()
{
    auto ecs = ecs::create<Components<Position, Velocity, RenderComponent>>(PhysicsSystem{}, RenderingSystem{});
    sf::CircleShape shape { 10.f };
    std::array<sf::Color, 3> colors {  }
    shape.setFillColor(sf::Color(100, 250, 50));
    std::vector<ecs::Entity> entities {};
    for(int i = -10; i <= 10; ++i)
    {
        auto e = ecs.createEntity();
        ecs.add<Position>(e, { i, 0, 100 });
        ecs.add<Velocity>(e, { i % 2 == 0 ? 2 : -2, 0, 0 });
        ecs.add<RenderComponent>(e, &shape, colors[(i+10) % 3]);
        entities.push_back(e);
    }
    for(auto i = 0; i < 8; ++i)
        std::next_permutation(std::begin(entities), std::end(entities));

    sf::RenderWindow window(sf::VideoMode(800, 600), "My window");
    sf::Clock clock;
    auto i = 0;
    float total_time{};
    while (window.isOpen())
    {
        auto dt = clock.restart().asSeconds();
        total_time += dt;
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear(sf::Color::Black);

        ecs.update(dt);
        
        if (total_time > i)
        {
            ++i;
            if (i % 5 == 0)
            {
                ecs.destroy(entities.pop_back());
                ecs.remove<RenderComponent>(entities.pop_back());
            }
        }

        window.display();
    }

    return 0;
}

void PhysicsSystem::update(double dt, Position& p, Velocity& v)
{
    p.x += v.x * dt;
    p.y += v.y * dt;
    v.y -= g * dt;
}

RenderingSystem::RenderingSystem(sf::RenderWindow& window)
: m_window { window }
{}

void RenderingSystem::update(RenderInfos const& render_infos, Position const& p)
{
    render_infos.shape->setPosition(sf::Vector2f{ p.x, p.y });
    render_infos.shape->setFillColor(render_infos.color);
    m_window.draw(*render_infos.shape);
}

void PositionShufflingSystem::update(ecs::ComponentRange const& positions)
{
    std::next_permutation(std::begin(positions), std::end(positions));
    auto c1 = std::begin(positions);
    auto c2 = ++e1;
    std::swap(*c1, *c2);
}
