# SimplECSpp

A simple Entity Component System for C++.

## How to build and run the vcl example

```sh
git clone --recursive https://gitlab.com/mehdidou99/simplecspp.git
cd examples
cp -r ecs inf443_vcl/scenes/3D_graphics/
cd inf443_vcl
# edit scenes/current_scene.hpp with #define SCENE_SPHERE_GRAVITY_FLOOR_WITH_ECS
# add #include "scenes/3D_graphics/ecs/example_sphere_gravity.hpp" to scenes/scenes.hpp
make
./pgm
```
