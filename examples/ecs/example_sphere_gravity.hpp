#pragma once

#include "main/scene_base/base.hpp"

#include "ECS/ecs.hpp"

#ifdef SCENE_SPHERE_GRAVITY_FLOOR_WITH_ECS

struct Position
{
    Position(vcl::vec3 const& p_) : p{p_} {}
    Position() = default;
    vcl::vec3 p;
};

struct Speed
{
    Speed(vcl::vec3 const& v_) : v{v_} {}
    Speed() = default;
    vcl::vec3 v;
};

struct RenderInfos
{
    RenderInfos(vcl::mesh_drawable& mesh_, GLuint shader_) : mesh{&mesh_}, shader{shader_} {}
    RenderInfos() = default;
    vcl::mesh_drawable* mesh { nullptr };
    GLuint shader;
};

struct CollisionInfos
{
    CollisionInfos(float r) : collisionSphereRadius{r}{}
    CollisionInfos() = default;
    float collisionSphereRadius;
};

struct Force
{
    Force(vcl::vec3 const& f_) : f{f_} {}
    Force() = default;
    vcl::vec3 f;
};

struct PhysicsInfos
{
    PhysicsInfos(float m_) : m{m_}{}
    PhysicsInfos() = default;
    float m;
};

using PositionComponent = ecs::Component<Position>;
using SpeedComponent = ecs::Component<Speed>;
using RenderComponent = ecs::Component<RenderInfos>;
using CollisionComponent = ecs::Component<CollisionInfos>;
using ForceComponent = ecs::Component<Force>;
using PhysicsComponent = ecs::Component<PhysicsInfos>;

struct GravitySystem
{
    void update(ForceComponent& force, PhysicsComponent const& physicsInfos);

    using Requirements = ecs::Requirements<ecs::Auto>;
    using UpdatePolicy = ecs::OncePerEntity;

    vcl::vec3 g { 0, 0, 9.81 };
};

struct PhysicsSystem
{
    void update(float dt, PositionComponent& p, SpeedComponent& v, ForceComponent const& force, PhysicsComponent const& physicsInfos);

    using Requirements = ecs::Requirements<ecs::Auto>;
    using UpdatePolicy = ecs::OncePerEntity;

    double time_step;
};

struct MyEcs;

class DeadEntityCollector
{
public:
    void update(PositionComponent const& p);

    void destroyAll(MyEcs& myEcs);

    using Requirements = ecs::Requirements<ecs::Auto>;
    using UpdatePolicy = ecs::OncePerEntity;

private:
    std::list<ecs::Entity> m_entitiesToDestroy;
};

struct CollisionSystem
{
    void update(PositionComponent& p, SpeedComponent& v);

    using Requirements = ecs::Requirements<ecs::Auto>;
    using UpdatePolicy = ecs::OncePerEntity;

    float groundRadius;
    float slowdownRate;
};

struct RenderSystem
{
    void update(RenderComponent& renderComponent, PositionComponent const& p);

    using Requirements = ecs::Requirements<ecs::Auto>;
    using UpdatePolicy = ecs::OncePerEntity;
    scene_structure* scene { nullptr };
};

struct MyEcs : ecs::ecs<ecs::Components<PositionComponent, SpeedComponent, RenderComponent, CollisionComponent, ForceComponent, PhysicsComponent>
                        , ecs::Systems<GravitySystem, PhysicsSystem, DeadEntityCollector, CollisionSystem, RenderSystem>>
{};

struct scene_model : scene_base
{
    void setup_data(std::map<std::string,GLuint>& shaders, scene_structure& scene, gui_structure& gui);
    void frame_draw(std::map<std::string,GLuint>& shaders, scene_structure& scene, gui_structure& gui);

    vcl::mesh_drawable sphere; // Visual representation of a particle - a sphere
    const float r = 0.05f;
    const float m = 0.01f; // particle mass
    vcl::mesh_drawable ground; // Visual representation of the ground - a disc
    vcl::timer_event timer;    // Timer allowing to indicate periodic events

    MyEcs m_ecs;
    ecs::Entity createParticle(GLuint shader);
    bool gravity { true };
};

#endif
