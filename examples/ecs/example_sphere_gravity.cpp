
#include "example_sphere_gravity.hpp"

#include <random>

#ifdef SCENE_SPHERE_GRAVITY_FLOOR_WITH_ECS

using namespace vcl;

// Generator for uniform random number
std::default_random_engine generator;
std::uniform_real_distribution<float> distrib(0.0,1.0);

void GravitySystem::update(ForceComponent& force, PhysicsComponent const& physicsInfos)
{
    force.f = - physicsInfos.m * g;
}

void PhysicsSystem::update(float dt, PositionComponent& p, SpeedComponent& v, ForceComponent const& force, PhysicsComponent const& physicsInfos)
{
    v.v += dt*force.f/physicsInfos.m;
    p.p += dt*v.v;
}

void DeadEntityCollector::update(PositionComponent const& p)
{
    if(p.p.z < -3)
        m_entitiesToDestroy.push_back(p.entity());
}

void DeadEntityCollector::destroyAll(MyEcs& myEcs)
{
    for(auto entity : m_entitiesToDestroy)
        myEcs.destroy(entity);
    m_entitiesToDestroy.clear();
}

void CollisionSystem::update(PositionComponent& p, SpeedComponent& v)
{
    if(p.p.z < 0 && std::sqrt(std::pow(p.p.x, 2) + std::pow(p.p.y, 2)) <= groundRadius)
    {
        v.v.z = std::abs(v.v.z);
        v.v *= slowdownRate;
        p.p.z = 0;
    }
}

void RenderSystem::update(RenderComponent& renderComponent, PositionComponent const& p)
{
    assert(scene);
    if(!renderComponent.mesh) return;
    renderComponent.mesh->uniform.transform.translation = p.p;
    draw(*renderComponent.mesh, scene->camera, renderComponent.shader);
}

static void set_gui(timer_event& timer, bool& gravity);

void scene_model::setup_data(std::map<std::string,GLuint>& , scene_structure& scene, gui_structure& gui)
{
    // Default camera settings
    scene.camera.camera_type = camera_control_spherical_coordinates;
    scene.camera.scale = 5.0f;
    scene.camera.apply_rotation(0,0, -2.0f,1.2f);

    // Default gui display
    gui.show_frame_worldspace = true;
    gui.show_frame_camera = false;

    // Create mesh for particles represented as spheres
    sphere = mesh_primitive_sphere(r);
    sphere.uniform.color = {0.6f, 0.6f, 1.0f};

    // Create mesh for the ground displayed as a disc
    ground = mesh_primitive_disc(2.0f, {0,0,-r}, {0,0,1}, 80);
    auto& collisionSystem = m_ecs.getSystem<CollisionSystem>();
    collisionSystem.groundRadius = 2.0f;
    collisionSystem.slowdownRate = 0.8;
    ground.uniform.color = {1,1,1};

    // Delay between emission of a new particles
    timer.periodic_event_time_step = 0.2f;
}

ecs::Entity scene_model::createParticle(GLuint shader)
{
    auto e = m_ecs.createEntity();
    m_ecs.add<PositionComponent>(e, vec3 { 0, 0, 0 });
    const float theta = 2*3.14f*distrib(generator);
    m_ecs.add<SpeedComponent>(e, vec3 { std::cos(theta), std::sin(theta), 5.0f } );
    m_ecs.add<RenderComponent>(e, sphere, shader);
    m_ecs.add<CollisionComponent>(e, r);
    m_ecs.add<ForceComponent>(e);
    m_ecs.add<PhysicsComponent>(e, m);
    return e;
}

void scene_model::frame_draw(std::map<std::string,GLuint>& shaders, scene_structure& scene, gui_structure& )
{
    const float dt = timer.update(); // dt: Elapsed time between last frame
    set_gui(timer, gravity);


    // Emission of new particle if needed
    const bool is_new_particle = timer.event;
    if( is_new_particle )
    {
        createParticle(shaders["mesh"]);
        /*particle_structure new_particle;
        const vec3 p0 = {0,0,0};

        // Initial speed is random. (x,z) components are uniformly distributed along a circle.
        const float theta     = 2*3.14f*distrib(generator);
        const vec3 v0 = vec3( std::cos(theta), std::sin(theta), 5.0f);

        particles.push_back({p0,v0});*/
    }


    // Evolve position of particles
    /*const vec3 g = {0.0f,0.0f,-9.81f};
    for(particle_structure& particle : particles)
    {
        vec3& p = particle.p;
        vec3& v = particle.v;

        const vec3 F = m*g;

        // Numerical integration
        v = v + dt*F/m;
        p = p + dt*v;
    }*/
    if(gravity)
        m_ecs.update<GravitySystem>();
    m_ecs.update<PhysicsSystem>(dt);


    // Remove particles that are too low
    /*for(auto it = particles.begin(); it!=particles.end(); ++it)
        if( it->p.z < -3)
            it = particles.erase(it);*/
    m_ecs.update<DeadEntityCollector>();
    m_ecs.getSystem<DeadEntityCollector>().destroyAll(m_ecs);

    /*for(auto& particle : particles)
    {
        if(particle.p.z < 0 && std::sqrt(std::pow(particle.p.x, 2) + std::pow(particle.p.y, 2)) <= ground_radius)
        {
            particle.v.z = std::abs(particle.v[2]);
            particle.v *= slowdown_rate;
            particle.p.z = 0;
        }
    }*/
    m_ecs.update<CollisionSystem>();


    // Display particles
    /*for(particle_structure& particle : particles)
    {
        sphere.uniform.transform.translation = particle.p;
        draw(sphere, scene.camera, shaders["mesh"]);
    }*/
    m_ecs.getSystem<RenderSystem>().scene = &scene;
    m_ecs.update<RenderSystem>();

    // Display ground
    draw(ground, scene.camera, shaders["mesh"]);
}



static void set_gui(timer_event& timer, bool& gravity)
{
    // Can set the speed of the animation
    float scale_min = 0.05f;
    float scale_max = 2.0f;
    ImGui::SliderScalar("Time scale", ImGuiDataType_Float, &timer.scale, &scale_min, &scale_max, "%.2f s");

    // Start and stop animation
    if (ImGui::Button("Stop"))
        timer.stop();
    if (ImGui::Button("Start"))
        timer.start();
    if (ImGui::Button("Stop Gravity"))
        gravity = false;
    if (ImGui::Button("Start Gravity"))
        gravity = true;
}


#endif
