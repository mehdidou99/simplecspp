#ifndef SYSTEM_POOL_HPP_INCLUDED
#define SYSTEM_POOL_HPP_INCLUDED

#include <tuple>
#include "Signature.hpp"
#include "SystemUpdater.hpp"

namespace ecs
{
    template<typename ComponentPoolT, typename ... SystemsT>
    class SystemPool
    {
    public:
        using SignatureType = signature_type<ComponentPoolT>;
    public:
        template<typename ... Args>
        SystemPool(ComponentPoolT& components, Args&& ... args);

        template<typename SystemT, typename ... Args>
        void update(Args&& ... args);

        void onEntityDestroyed(Entity entity);
        void onEntitySignatureChanged(Entity entity, SignatureType signature);

        template<typename SystemT>
        auto& get() { return std::get<SystemT>(m_systems); }

        template<typename SystemT>
        auto const& get() const { return std::get<SystemT>(m_systems); }

    private:
        ComponentPoolT& m_components;
        std::tuple<SystemsT...> m_systems;
        std::tuple<SystemUpdater<SystemsT, ComponentPoolT>...> m_systemUpdaters;
    };
}

#include "utils/utilities.hpp"

namespace ecs
{
    template<typename ComponentPoolT, typename ... SystemsT>
    template<typename ... Args>
    SystemPool<ComponentPoolT, SystemsT...>::SystemPool(ComponentPoolT& components, Args&& ... args)
    : m_components { components }
    , m_systems { std::forward<Args>(args)... }
    , m_systemUpdaters { { m_components, std::get<SystemsT>(m_systems)}... }
    {}

    template<typename ComponentPoolT, typename ... SystemsT>
    template<typename SystemT, typename ... Args>
    void SystemPool<ComponentPoolT, SystemsT...>::update(Args&& ... args)
    {
        return std::get<SystemUpdater<SystemT, ComponentPoolT>>(m_systemUpdaters).update(std::forward<Args>(args)...);
    }

    template<typename ComponentPoolT, typename ... SystemsT>
    void SystemPool<ComponentPoolT, SystemsT...>::onEntityDestroyed(Entity entity)
    {
        utils::for_each(m_systemUpdaters, [entity](auto& updater) { updater.onEntityDestroyed(entity); });
    }

    template<typename ComponentPoolT, typename ... SystemsT>
    void SystemPool<ComponentPoolT, SystemsT...>::onEntitySignatureChanged(Entity entity, SignatureType signature)
    {
        utils::for_each(m_systemUpdaters, [entity, signature](auto& updater) { updater.onEntitySignatureChanged(entity, signature); });
    }
}

#endif // !SYSTEM_POOL_HPP_INCLUDED