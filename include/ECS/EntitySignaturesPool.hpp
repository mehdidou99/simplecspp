#ifndef ENTITY_SIGNATURES_POOL_HPP_INCLUDED
#define ENTITY_SIGNATURES_POOL_HPP_INCLUDED

#include <map>
#include "Entity.hpp"
#include "Signature.hpp"

namespace ecs
{
    template<typename ... ComponentsT>
    class EntitySignaturesPool
    {
    public:
        using SignatureType = Signature<ComponentsT...>;

    public:
        void onEntityCreated(Entity entity) { m_signatures.emplace(entity, SignatureType{}); }
        void onEntityDestroyed(Entity entity) { m_signatures.erase(entity); }

        template<typename ComponentT>
        SignatureType onEntityComponentAdded(Entity entity);

        template<typename ComponentT>
        SignatureType onEntityComponentRemoved(Entity entity);

        SignatureType getSignature(Entity entity);

    private:
        std::map<Entity, SignatureType> m_signatures;
    };
}

namespace ecs
{
    template<typename ... ComponentsT>
    template<typename ComponentT>
    typename EntitySignaturesPool<ComponentsT...>::SignatureType EntitySignaturesPool<ComponentsT...>::onEntityComponentAdded(Entity entity)
    {
        auto sig = m_signatures.find(entity);
        assert(sig != std::end(m_signatures) && "Entity doesn't exist.");
        SignatureChanger<SignatureType>::template add<ComponentT>(sig->second);
        return sig->second;
    }

    template<typename ... ComponentsT>
    template<typename ComponentT>
    typename EntitySignaturesPool<ComponentsT...>::SignatureType EntitySignaturesPool<ComponentsT...>::onEntityComponentRemoved(Entity entity)
    {
        auto sig = m_signatures.find(entity);
        assert(sig != std::end(m_signatures) && "Entity doesn't exist.");
        SignatureChanger<SignatureType>::template remove<ComponentT>(sig->second);
        return sig->second;
    }

    template<typename ... ComponentsT>
    typename EntitySignaturesPool<ComponentsT...>::SignatureType EntitySignaturesPool<ComponentsT...>::getSignature(Entity entity)
    {
        auto sig = m_signatures.find(entity);
        assert(sig != std::end(m_signatures) && "Entity doesn't exist.");
        return sig->second;
    }
}

#endif // !ENTITY_SIGNATURES_POOL_HPP_INCLUDED