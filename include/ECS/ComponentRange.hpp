#ifndef COMPONENT_RANGE_HPP_INCLUDED
#define COMPONENT_RANGE_HPP_INCLUDED

#include "ComponentArray.hpp"

namespace ecs
{
    template<typename ComponentT>
    using ComponentRange = ComponentArray<ComponentT>;
}

#endif // !COMPONENT_RANGE_HPP_INCLUDED