#ifndef ENTITY_SPECIFIC_RANGE_HPP_INCLUDED
#define ENTITY_SPECIFIC_RANGE_HPP_INCLUDED

#include <vector>
#include <functional>

namespace ecs
{
    template<typename ComponentT>
    using EntitySpecificRange = std::vector<std::reference_wrapper<ComponentT>>;

    template<typename ComponentT>
    using ConstEntitySpecificRange = std::vector<std::reference_wrapper<const ComponentT>>;
}

#endif // !ENTITY_SPECIFIC_RANGE_HPP_INCLUDED