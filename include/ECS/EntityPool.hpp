#ifndef ENTITY_POOL_HPP_INCLUDED
#define ENTITY_POOL_HPP_INCLUDED

#include <set>

#include "Entity.hpp"

namespace ecs
{
    using EntityPool = std::set<Entity>;
}

#endif // !ENTITY_POOL_HPP_INCLUDED