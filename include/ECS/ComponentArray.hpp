#ifndef COMPONENT_ARRAY_HPP_INCLUDED
#define COMPONENT_ARRAY_HPP_INCLUDED

#include <vector>
#include <unordered_map>
#include <cassert>
#include <algorithm>

#include "default_values.hpp"

#include "Component.hpp"
#include "EntitySpecificRange.hpp"

namespace ecs
{
    template<typename ComponentT>
    class ComponentArray
    {
    private:
        using MyType = ComponentArray<ComponentT>;
        using UnderlyingArray = std::vector<ComponentT>;

    public:
        using value_type = typename UnderlyingArray::value_type;
        using allocator_type = typename UnderlyingArray::allocator_type;
        using size_type = typename UnderlyingArray::size_type;
        using difference_type = typename UnderlyingArray::difference_type;
        using reference = typename UnderlyingArray::reference;
        using const_reference = typename UnderlyingArray::const_reference;
        using pointer = typename UnderlyingArray::pointer;
        using const_pointer = typename UnderlyingArray::const_pointer;
        using iterator = typename UnderlyingArray::iterator;
        using const_iterator = typename UnderlyingArray::const_iterator;
        using reverse_iterator = typename UnderlyingArray::reverse_iterator;
        using const_reverse_iterator = typename UnderlyingArray::const_reverse_iterator;

    private:
        using EntityToIndexMap = std::unordered_multimap<Entity, size_type>;
        using IndexToEntityMap = std::unordered_map<size_type, Entity>;

    public:
        ComponentArray(size_type initial_reserved_size = defaults::initialComponentArraySize);

        ComponentArray(ComponentArray const&) = default;
        ComponentArray& operator=(ComponentArray const& rhs) { ComponentArray rhsCopy { rhs }; swap(rhsCopy); return *this; }

        ComponentArray(ComponentArray&&) = default;
        ComponentArray& operator=(ComponentArray&& rhs) = default;

        void swap(ComponentArray& rhs) { std::swap(m_components, rhs.m_components); std::swap(m_entityToIndexMap, rhs.m_entityToIndexMap); std::swap(m_indexToEntityMap, rhs.m_indexToEntityMap); }

        template<typename ... Args>
        ComponentT& add(Entity entity, Args&&... args);

        ComponentT& add(ComponentT const& c);
        ComponentT& add(ComponentT&& c);

        void removeComponents(Entity entity);

        void onEntityDestroyed(Entity entity);

        ComponentT& get(Entity entity);
        ComponentT const& get(Entity entity) const;

        EntitySpecificRange<ComponentT> getAll(Entity entity);
        ConstEntitySpecificRange<ComponentT> getAll(Entity entity) const;
        ConstEntitySpecificRange<ComponentT> cgetAll(Entity entity) const;

        bool has(Entity entity) const { return m_entityToIndexMap.find(entity) != std::end(m_entityToIndexMap); }

        ComponentT& operator[](size_type idx) { return m_components[idx]; }
        ComponentT const& operator[](size_type idx) const { return m_components[idx]; }

        bool empty() const noexcept { return m_components.empty(); }
        size_type size() const noexcept { return m_components.size(); }

        iterator begin() noexcept { return m_components.begin(); }
        iterator end() noexcept { return m_components.end(); }
        const_iterator begin() const noexcept { return m_components.begin(); }
        const_iterator end() const noexcept { return m_components.end(); }
        const_iterator cbegin() noexcept { return m_components.cbegin(); }
        const_iterator cend() noexcept { return m_components.cend(); }

        reverse_iterator rbegin() noexcept { return m_components.rbegin(); }
        reverse_iterator rend() noexcept { return m_components.rend(); }
        const_reverse_iterator rbegin() const noexcept { return m_components.rbegin(); }
        const_reverse_iterator rend() const noexcept { return m_components.rend(); }
        const_reverse_iterator crbegin() const noexcept { return m_components.crbegin(); }
        const_reverse_iterator crend() const noexcept { return m_components.crend(); }

        UnderlyingArray& components() { return m_components; }
        UnderlyingArray const& components() const { return m_components; }
        std::unordered_multimap<Entity, size_t> const& entityToIndexMap() const { return m_entityToIndexMap; }
        std::unordered_map<size_t, Entity> const& indexToEntityMap() const { return m_indexToEntityMap; }

    private:
        void updateMapsAfterAdd(Entity e, size_type new_index);
        void updateEntityToIndexMapAfterRemoval(Entity entity, size_type old_index, size_type new_index);

    private:
        UnderlyingArray m_components;
        EntityToIndexMap m_entityToIndexMap;
    	IndexToEntityMap m_indexToEntityMap;
    };
}

#include <algorithm>

namespace ecs
{
    template<typename ComponentT>
    ComponentArray<ComponentT>::ComponentArray(size_type initial_reserved_size)
    : m_components {}
    , m_entityToIndexMap {}
    , m_indexToEntityMap {}
    {
        m_components.reserve(initial_reserved_size);
    }

    template<typename ComponentT>
    template<typename ... Args>
    ComponentT& ComponentArray<ComponentT>::add(Entity entity, Args&&... args)
    {
        auto new_index = std::size(m_components);
        auto& component = m_components.emplace_back(entity, std::forward<Args>(args)...);
        updateMapsAfterAdd(entity, new_index);

        return component;
    }

    template<typename ComponentT>
    ComponentT& ComponentArray<ComponentT>::add(ComponentT const& c)
    {
        auto new_index = std::size(m_components);
        auto& component = m_components.emplace_back(c);
        updateMapsAfterAdd(c.entity(), new_index);

        return component;
    }

    template<typename ComponentT>
    ComponentT& ComponentArray<ComponentT>::add(ComponentT&& c)
    {
        auto new_index = std::size(m_components);
        auto& component = m_components.emplace_back(std::move(c));
        updateMapsAfterAdd(c.entity(), new_index);

        return component;
    }

    template<typename ComponentT>
    void ComponentArray<ComponentT>::removeComponents(Entity entity)
    {
        std::vector<decltype(m_entityToIndexMap.equal_range(entity).first->second)> indicesToEntityComponents{};
        {
            auto indices = m_entityToIndexMap.equal_range(entity);
            assert(indices.first != std::end(m_entityToIndexMap) && "There is no component to remove.");
            std::transform(indices.first, indices.second, std::back_inserter(indicesToEntityComponents), [](auto const& mapValue){ return mapValue.second; });
            std::sort(std::begin(indicesToEntityComponents), std::end(indicesToEntityComponents));
        }

        auto findNextPotentialMoveTarget = [&m_components = m_components, entity](auto const& searchBegin) 
        {
            return std::find_if_not(searchBegin, std::rend(m_components), [entity](ComponentT const& c) { return c.entity() == entity; });
        };
        auto indexToIterator = [&m_components = m_components](size_type index)
        {
            auto res = std::begin(m_components);
            std::advance(res, index);
            return res;
        };

        auto reversedNextPotentialMoveTarget = findNextPotentialMoveTarget(std::rbegin(m_components));
        auto componentIndex = std::begin(indicesToEntityComponents);
        while(componentIndex != std::end(indicesToEntityComponents) && reversedNextPotentialMoveTarget != std::rend(m_components) && indexToIterator(*componentIndex) < reversedNextPotentialMoveTarget.base())
        {
            auto nextPotentialMoveTarget = reversedNextPotentialMoveTarget.base() - 1;
            auto nextPotentialMoveTargetIndex = static_cast<size_type>(std::distance(std::begin(m_components), nextPotentialMoveTarget));
            auto nextPotentialMoveTargetEntity = m_indexToEntityMap[nextPotentialMoveTargetIndex];

            m_components[*componentIndex].assignAsWhole(std::move(*nextPotentialMoveTarget));
            updateEntityToIndexMapAfterRemoval(nextPotentialMoveTargetEntity, nextPotentialMoveTargetIndex, *componentIndex);
            m_indexToEntityMap[*componentIndex] = nextPotentialMoveTargetEntity;
            
            m_indexToEntityMap.erase(nextPotentialMoveTargetIndex);
            ++reversedNextPotentialMoveTarget;
            reversedNextPotentialMoveTarget = findNextPotentialMoveTarget(reversedNextPotentialMoveTarget);
            ++componentIndex;
        }
        auto numToErase = static_cast<size_type>(std::size(indicesToEntityComponents));
        auto firstToErase = indexToIterator(std::size(m_components) - numToErase);
        m_components.erase(firstToErase, std::end(m_components));
        m_entityToIndexMap.erase(entity);
    }

    template<typename ComponentT>
    void ComponentArray<ComponentT>::onEntityDestroyed(Entity entity)
    {
        if (m_entityToIndexMap.find(entity) != std::end(m_entityToIndexMap))
            removeComponents(entity);
    }

    template<typename ComponentT>
    ComponentT& ComponentArray<ComponentT>::get(Entity entity)
    {
        return 
            const_cast<ComponentT&>(
                static_cast<MyType const&>(*this).get(entity)
            );
    }
    template<typename ComponentT>
    ComponentT const& ComponentArray<ComponentT>::get(Entity entity) const
    {
        auto idx = m_entityToIndexMap.find(entity);
        assert(idx != std::end(m_entityToIndexMap) && "The entity doesn't have any component of this type.");
        return m_components[idx->second];
    }

    template<typename ComponentT>
    EntitySpecificRange<ComponentT> ComponentArray<ComponentT>::getAll(Entity entity)
    {
        auto indices = m_entityToIndexMap.equal_range(entity);
        assert(indices.first != std::end(m_entityToIndexMap) && "The entity doesn't have any component of this type.");

        EntitySpecificRange<ComponentT> res {};
        std::transform(indices.first, indices.second, std::back_inserter(res), [this](auto const& entry) { return std::ref(m_components[entry.second]); });

        return res;
    }
    template<typename ComponentT>
    ConstEntitySpecificRange<ComponentT> ComponentArray<ComponentT>::getAll(Entity entity) const
    {
        auto indices = m_entityToIndexMap.equal_range(entity);
        assert(indices.first != std::end(m_entityToIndexMap) && "The entity doesn't have any component of this type.");

        ConstEntitySpecificRange<ComponentT> res {};
        std::transform(indices.first, indices.second, std::back_inserter(res), [this](auto const& entry) { return std::cref(m_components[entry.second]); });

        return res;
    }
    template<typename ComponentT>
    ConstEntitySpecificRange<ComponentT> ComponentArray<ComponentT>::cgetAll(Entity entity) const
    {
        auto indices = m_entityToIndexMap.equal_range(entity);
        assert(indices.first != std::end(m_entityToIndexMap) && "The entity doesn't have any component of this type.");

        ConstEntitySpecificRange<ComponentT> res {};
        std::transform(indices.first, indices.second, std::back_inserter(res), [this](auto const& entry) { return std::cref(m_components[entry.second]); });

        return res;
    }

    template<typename ComponentT>
    void ComponentArray<ComponentT>::updateMapsAfterAdd(Entity entity, size_type new_index)
    {
        m_entityToIndexMap.emplace(entity, new_index);
        m_indexToEntityMap[new_index] = entity;
    }

    template<typename ComponentT>
    void ComponentArray<ComponentT>::updateEntityToIndexMapAfterRemoval(Entity moved_entity, size_type old_index, size_type new_index)
    {
        auto nextPotentialMoveTargetEntry = std::find(std::begin(m_entityToIndexMap), std::end(m_entityToIndexMap), typename EntityToIndexMap::value_type{ moved_entity, old_index });
        assert(nextPotentialMoveTargetEntry != std::end(m_entityToIndexMap) && "Violated invariant : entity2index and index2entity maps are incoherent.");
        nextPotentialMoveTargetEntry->second = new_index;
    }
}

#endif // !COMPONENT_ARRAY_HPP_INCLUDED