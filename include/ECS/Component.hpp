#ifndef COMPONENT_HPP_INCLUDED
#define COMPONENT_HPP_INCLUDED

#include <utility>

#include "Entity.hpp"
#include "utils/utilities.hpp"

namespace ecs
{
    template<typename UnderlyingT>
    class Component : public UnderlyingT // Inheritance in that direction and not the other way around so that the user needn't to create the constructors that take an entity
    {
    public:
        template<typename ... Args>
        explicit Component(Entity entity, Args&& ... args);

        Component(Component const& rhs) = default;
        Component(Entity entity, Component const& rhs);
        Component& operator=(Component const& rhs);
        Component& assignAsWhole(Component const& rhs);

        Component(Component&& rhs) = default;
        Component(Entity entity, Component&& rhs);
        Component& operator=(Component&& rhs);
        Component& assignAsWhole(Component&& rhs);

        Entity entity() const;

    private:
        UnderlyingT& asUnderlying() { return static_cast<UnderlyingT&>(*this); }
        UnderlyingT const& asUnderlying() const { return static_cast<UnderlyingT const&>(*this); }

    private:
        Entity m_entity;
    };
}

namespace ecs
{
    template<typename UnderlyingT>
    template<typename ... Args>
    Component<UnderlyingT>::Component(Entity entity, Args&& ... args)
    : UnderlyingT(std::forward<Args>(args)...)
    , m_entity { entity }
    {}

    template<typename UnderlyingT>
    Component<UnderlyingT>::Component(Entity entity, Component const& rhs)
    : UnderlyingT { rhs }
    , m_entity { entity }
    {}

    template<typename UnderlyingT>
    Component<UnderlyingT>& Component<UnderlyingT>::operator=(Component const& rhs)
    {
        UnderlyingT::operator=(rhs);
        return *this;
    }

    template<typename UnderlyingT>
    Component<UnderlyingT>& Component<UnderlyingT>::assignAsWhole(Component<UnderlyingT> const& rhs)
    {
        UnderlyingT::operator=(rhs);
        m_entity = rhs.entity();
        return *this;
    }

    template<typename UnderlyingT>
    Component<UnderlyingT>::Component(Entity entity, Component<UnderlyingT>&& rhs)
    : UnderlyingT(std::move(rhs))
    , m_entity { entity }
    {}

    template<typename UnderlyingT>
    Component<UnderlyingT>& Component<UnderlyingT>::operator=(Component<UnderlyingT>&& rhs)
    {
        UnderlyingT::operator=(std::move(rhs));
        return *this;
    }

    template<typename UnderlyingT>
    Component<UnderlyingT>& Component<UnderlyingT>::assignAsWhole(Component<UnderlyingT>&& rhs)
    {
        UnderlyingT::operator=(std::move(rhs));
        m_entity = std::move(rhs.entity());
        return *this;
    }

    template<typename UnderlyingT>
    Entity Component<UnderlyingT>::entity() const
    {
        return m_entity;
    }
}

#endif // !COMPONENT_HPP_INCLUDED