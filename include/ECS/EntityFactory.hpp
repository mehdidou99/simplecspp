#ifndef ENTITY_FACTORY_HPP_INCLUDED
#define ENTITY_FACTORY_HPP_INCLUDED

#include <queue>
#include <map>

#include "Entity.hpp"

namespace ecs
{
    class EntityFactory
    {
    public:
        inline Entity create();
        inline void onEntityDestroyed(Entity entity);

    private:
        std::queue<Entity> m_available_entities;
        Entity m_next_free_entity { 0 };
    };
}

#include <cassert>

namespace ecs
{
    inline Entity EntityFactory::create()
    {
        if (m_available_entities.empty())
            return m_next_free_entity++;

        auto res = m_available_entities.front();
        m_available_entities.pop();
        return res;
    }

    inline void EntityFactory::onEntityDestroyed(Entity entity)
    {
        assert(entity < m_next_free_entity && "Entity doesn't exist");
        m_available_entities.push(entity);
    }
}

#endif // !ENTITY_FACTORY_HPP_INCLUDED