#ifndef EVENTSENDER_HPP_INCLUDED
#define EVENTSENDER_HPP_INCLUDED

#include <unordered_map>
#include <string>
#include <functional>

namespace EventPassingTraits
{
    template<typename T>
    struct ByValue
    {
        using type = T;
    };

    template<typename T>
    struct ByConstReference
    {
        using type = T const&;
    };
}

template<typename T, template<typename> typename EventPassingTrait = EventPassingTraits::ByConstReference>
class EventSender
{
public:
    using ArgType = typename EventPassingTrait<T>::type;
public:
    template<typename Callback>
    bool subscribe(std::string const& name, Callback&& callback)
    {
        auto result = m_callbacks.insert({ name, std::forward<Callback>(callback) });
        return result.second;
    }

    bool unsubscribe(std::string const& name)
    {
        auto numErased = m_callbacks.erase(name);
        return numErased != 0;
    }

    void send(ArgType arg)
    {
        for(auto const& callback : m_callbacks)
        {
            callback.second(arg);
        }
    }

private:
    std::unordered_map<std::string, std::function<void(ArgType)>> m_callbacks;
};

#endif // !EVENTSENDER_HPP_INCLUDED