#ifndef TYPELIST_HPP_INCLUDED
#define TYPELIST_HPP_INCLUDED

// Thanks to Andrei Alexandrescu (Loki)

#include <cstdint>

template<typename ... Ts>
struct Typelist;

namespace tl
{
    template<typename TL> struct length{};
    template<typename TL>
    constexpr std::size_t length_v = length<TL>::value;

    template<typename TL, std::size_t index> struct get;
    template<typename TL, std::size_t index>
    using get_t = typename get<TL, index>::type;

    template<typename TL, typename T> struct index_of;
    template<typename TL, typename T>
    constexpr int index_of_v = index_of<TL, T>::value;

    template<typename TL1, typename TL2> struct concat;
    template<typename TL1, typename TL2>
    using concat_t = typename concat<TL1, TL2>::type;

    template<typename ... TLs> struct concat_all;
    template<typename ... TLs>
    using concat_all_t = typename concat_all<TLs...>::type;

    template<typename T, typename TL> struct remove_all;
    template<typename T, typename TL>
    using remove_all_t = typename remove_all<T, TL>::type;

    template<typename TL, template<typename ListElem> typename Modifier> struct for_each;
    template<typename TL, template<typename ListElem> typename Modifier>
    using for_each_t = typename for_each<TL, Modifier>::type;

    template<template<typename...> typename TypedPack, typename TL> struct to_typed_pack;
    template<template<typename...> typename TypedPack, typename TL>
    using to_typed_pack_t = typename to_typed_pack<TypedPack, TL>::type;
}

namespace tl
{
    template<>
    struct length<Typelist<>>
    {
        static constexpr std::size_t value = 0;
    };
    template<typename Head, typename ... Tail>
    struct length<Typelist<Head, Tail...>>
    {
        static constexpr std::size_t value = 1 + length<Typelist<Tail...>>::value;
    };

    template<typename Head, typename ... Ts>
    struct get<Typelist<Head, Ts...>, 0>
    {
        using type = Head;
    };
    template<typename Head, typename ... Tail, std::size_t index>
    struct get<Typelist<Head, Tail...>, index>
    {
        using type = typename get<Typelist<Tail...>, index-1>::type;
    };

    template<typename ... Tail, typename T>
    struct index_of<Typelist<T, Tail...>, T>
    {
        static constexpr int value = 0;
    };
    template<typename T>
    struct index_of<Typelist<>, T>
    {
        static constexpr int value = -1;
    };
    template<typename Head, typename ... Tail, typename T>
    struct index_of<Typelist<Head, Tail...>, T>
    {
    private:
        static constexpr int temp = index_of<Typelist<Tail...>, T>::value;
    public:
        static constexpr int value = temp == -1 ? -1 : 1 + temp;
    };

    template<typename ... Ts, typename ... Us>
    struct concat<Typelist<Ts...>, Typelist<Us...>>
    {
        using type = Typelist<Ts..., Us...>;
    };

    template<>
    struct concat_all<>
    {
        using type = Typelist<>;
    };
    template<typename HeadTL, typename ... TLs>
    struct concat_all<HeadTL, TLs...>
    {
        using type = concat_t<HeadTL, typename concat_all<TLs...>::type>;
    };

    template<typename T>
    struct remove_all<T, Typelist<>>
    {
        using type = Typelist<>;
    };
    template<typename T, typename Head, typename ... Tail>
    struct remove_all<T, Typelist<Head, Tail...>>
    {
        using type = concat_t<Typelist<Head>, typename remove_all<T, Typelist<Tail...>>::type>;
    };
    template<typename T, typename ... Tail>
    struct remove_all<T, Typelist<T, Tail...>>
    {
        using type = typename remove_all<T, Typelist<Tail...>>::type;
    };

    template<typename ... Ts, template<typename ListElem> typename Modifier>
    struct for_each<Typelist<Ts...>, Modifier>
    {
        using type = Typelist<typename Modifier<Ts>::type...>;
    };

    template<template<typename...> typename TypedPack, typename ... Ts>
    struct to_typed_pack<TypedPack, Typelist<Ts...>>
    {
        using type = TypedPack<Ts...>;
    };
}

#endif // !TYPELIST_HPP_INCLUDED
