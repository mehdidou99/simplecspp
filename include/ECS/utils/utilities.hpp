#ifndef UTILITIES_HPP_INCLUDED
#define UTILITIES_HPP_INCLUDED

namespace ecs
{
    namespace utils
    {
        template<typename T>
        using ObserverPtr = T*;

        template <class Tuple, class F>
        constexpr F for_each(Tuple&& t, F&& f); // Thanks to Jonathan Boccara (https://www.fluentcpp.com/2019/03/08/stl-algorithms-on-tuples/)

        constexpr std::size_t pow2(std::size_t e)
        {
            if (e == 0) return 1;
            if (e % 2 == 0)
            {
                auto pow = pow2(e / 2);
                return pow*pow;
            }
            if(e%2 == 1)
            {
                auto pow = pow2((e-1) / 2);
                return 2 * pow * pow;
            }
        }
    }
}

#include <utility>
#include <tuple>

namespace ecs
{
    namespace utils
    {
        template <class Tuple, class F, std::size_t... I>
        constexpr F for_each_impl(Tuple&& t, F&& f, std::index_sequence<I...>)
        {
            return (void)std::initializer_list<int>{(std::forward<F>(f)(std::get<I>(std::forward<Tuple>(t))),0)...}, f;
        }

        template <class Tuple, class F>
        constexpr F for_each(Tuple&& t, F&& f)
        {
            return for_each_impl(std::forward<Tuple>(t), std::forward<F>(f),
                                std::make_index_sequence<std::tuple_size<std::remove_reference_t<Tuple>>::value>{});
        }
    }
}

#endif // !UTILITIES_HPP_INCLUDED