#pragma once

#include "Typelist.hpp"

template<typename FunctionT>
struct extract_signature;

template<typename  ReturnT, typename ... Args>
struct extract_signature<ReturnT (*)(Args...)>
{
    using return_type = ReturnT;
    using args = Typelist<Args...>;
};

template<typename ClassT, typename  ReturnT, typename ... Args>
struct extract_signature<ReturnT (ClassT::*)(Args...)>
{
    using return_type = ReturnT;
    using args = Typelist<Args...>;
};
