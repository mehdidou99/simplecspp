#ifndef REQUIREMENT_GETTER_HPP_INCLUDED
#define REQUIREMENT_GETTER_HPP_INCLUDED

#include "ComponentRange.hpp"
#include "Requirements.hpp"

namespace ecs
{    
    template<typename Requirement, typename ComponentPoolT>
    struct ParameterGetter;
}

namespace ecs
{
    template<typename ComponentPoolT>
    struct ParameterGetter<CompatibleEntities, ComponentPoolT>
    {
        static EntityPool const& get(ComponentPoolT& /*components*/, EntityPool const& entities) { return entities; }
        static EntityPool const& get(Entity /*entity*/, ComponentPoolT& /*components*/, EntityPool const& entities) { return entities; }
    };

    template<typename UnderlyingT, typename ComponentPoolT>
    struct ParameterGetter<Component<UnderlyingT>, ComponentPoolT>
    {
        static Component<UnderlyingT>& get(Entity entity, ComponentPoolT& components, EntityPool const& /*entities*/) { return components.template get<Component<UnderlyingT>>(entity); }
    };

    template<typename ComponentT, typename ComponentPoolT>
    struct ParameterGetter<ComponentRange<ComponentT>, ComponentPoolT>
    {
        static ComponentRange<ComponentT>& get(ComponentPoolT& components, EntityPool const& /*entities*/) { return components.template getAll<ComponentT>(); }
        static ComponentRange<ComponentT>& get(Entity /*entity*/, ComponentPoolT& components, EntityPool const& /*entities*/) { return components.template getAll<ComponentT>(); }
    };
}

#endif // !REQUIREMENT_GETTER_HPP_INCLUDED