#ifndef SYSTEM_UPDATER_HPP_INCLUDED
#define SYSTEM_UPDATER_HPP_INCLUDED

#include "EntityPool.hpp"
#include "Signature.hpp"
#include "Requirements.hpp"
#include "Parameters.hpp"
#include "ParameterGetter.hpp"
#include "utils/extract_signature.hpp"

#include <type_traits>

namespace ecs
{
    struct OncePerUpdate{};
    struct OncePerEntity{};
    struct Manual{};

    template<typename SystemT, typename ComponentPoolT, typename ParametersT, typename RequirementsT>
    class SystemUpdaterImpl;

    template<typename SystemT>
    using StrippedParametersTypelist = tl::for_each_t<tl::for_each_t<typename extract_signature<decltype(&SystemT::update)>::args, std::remove_reference>, std::remove_cv>;

    template<typename SystemT, typename ComponentPoolT>
    using SystemUpdater = SystemUpdaterImpl<SystemT, ComponentPoolT, ProvidableParameters<StrippedParametersTypelist<SystemT>>, ExpandedRequirements<typename SystemT::Requirements, StrippedParametersTypelist<SystemT>>>;

    template<typename SystemT, typename ComponentPoolT, typename ... ParametersT, typename ... RequirementsT>
    class SystemUpdaterImpl<SystemT, ComponentPoolT, Parameters<ParametersT...>, Requirements<RequirementsT...>>
    {
    public:
        using SignatureType = signature_type<ComponentPoolT>;

    public:
        SystemUpdaterImpl(ComponentPoolT& components, SystemT& system);

        template<typename ... Args>
        auto update(Args&& ... args);

        void onEntityDestroyed(Entity entity) { m_entities.erase(entity); }
        bool onEntitySignatureChanged(Entity entity, SignatureType signature);

        EntityPool const& entities() const { return m_entities; }

    private:
        static constexpr auto signature = RequirementsToSignatureConverter<ComponentPoolT>::template convert<RequirementsT...>;

    private:
        EntityPool m_entities;
        ComponentPoolT& m_components;
        SystemT& m_system;
    };
}

#include "ParameterGetter.hpp"

namespace ecs
{
    template<typename SystemT, typename ComponentPoolT, typename ... ParametersT, typename ... RequirementsT>
    SystemUpdaterImpl<SystemT, ComponentPoolT, Parameters<ParametersT...>, Requirements<RequirementsT...>>::SystemUpdaterImpl(ComponentPoolT& components, SystemT& system)
    : m_entities {}
    , m_components { components }
    , m_system { system }
    {}

    template<typename SystemT, typename ComponentPoolT, typename ... ParametersT, typename ... RequirementsT>
    template<typename ... Args>
    auto SystemUpdaterImpl<SystemT, ComponentPoolT, Parameters<ParametersT...>, Requirements<RequirementsT...>>::update(Args&& ... args)
    {
        if constexpr(std::is_same_v<typename SystemT::UpdatePolicy, OncePerUpdate>)
        {
            return m_system.update(std::forward<Args>(args)..., ParameterGetter<ParametersT, ComponentPoolT>::get(m_components, entities())...);
        }
        else if constexpr (std::is_same_v<typename SystemT::UpdatePolicy, OncePerEntity>)
        {
            for(Entity e : m_entities)
            {
                m_system.update(std::forward<Args>(args)..., ParameterGetter<ParametersT, ComponentPoolT>::get(e, m_components, entities())...);
            }
        }
        else if constexpr (std::is_same_v<typename SystemT::UpdatePolicy, Manual>)
        {}
    }

    template<typename SystemT, typename ComponentPoolT, typename ... ParametersT, typename ... RequirementsT>
    bool SystemUpdaterImpl<SystemT, ComponentPoolT, Parameters<ParametersT...>, Requirements<RequirementsT...>>::onEntitySignatureChanged(Entity entity, SignatureType new_signature)
    {
        if ((new_signature & signature) == signature)
        {
            m_entities.insert(entity);
            return true;
        }
        else
        {
            m_entities.erase(entity);
            return false;
        }
    }
}

#endif // !SYSTEM_UPDATER_HPP_INCLUDED