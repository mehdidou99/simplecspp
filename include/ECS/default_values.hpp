#ifndef DEFAULT_VALUES_HPP_INCLUDED
#define DEFAULT_VALUES_HPP_INCLUDED

namespace ecs
{
    namespace defaults
    {
        constexpr std::size_t initialComponentArraySize { 1000 };
    }
}

#endif // !DEFAULT_VALUES_HPP_INCLUDED