#ifndef ENTITY_HPP_INCLUDED
#define ENTITY_HPP_INCLUDED

#include <cstdint>

namespace ecs
{
    using Entity = std::uint32_t;
}

#endif // !ENTITY_HPP_INCLUDED