#ifndef COMPONENT_POOL_HPP_INCLUDED
#define COMPONENT_POOL_HPP_INCLUDED

#include <tuple>

#include "ComponentArray.hpp"
#include "ComponentRange.hpp"
#include "utils/utilities.hpp"

namespace ecs
{
    template<typename ... ComponentsT>
    class ComponentPool
    {
    public:
        ComponentPool(std::size_t initial_reserved_size = defaults::initialComponentArraySize)
        : m_componentArrays { ComponentArray<ComponentsT>{ initial_reserved_size }... }
        {}

        template<typename ComponentT, typename ... Args>
        ComponentT& add(Entity entity, Args&&... args) { return std::get<ComponentArray<ComponentT>>(m_componentArrays).add(entity, std::forward<Args>(args)...); }

        template<typename ComponentT>
        void remove(Entity entity) { std::get<ComponentArray<ComponentT>>(m_componentArrays).remove(entity); }

        void onEntityDestroyed(Entity entity) { utils::for_each(m_componentArrays, [entity](auto& components) { components.onEntityDestroyed(entity); }); }

        template<typename ComponentT>
        ComponentT& get(Entity entity) { return std::get<ComponentArray<ComponentT>>(m_componentArrays).get(entity); }
        template<typename ComponentT>
        ComponentT const& get(Entity entity) const { return std::get<ComponentArray<ComponentT>>(m_componentArrays).get(entity); }

        template<typename ComponentT>
        EntitySpecificRange<ComponentT> getAll(Entity entity) { return std::get<ComponentArray<ComponentT>>(m_componentArrays).getAll(entity); }
        template<typename ComponentT>
        ConstEntitySpecificRange<ComponentT> getAll(Entity entity) const { return std::get<ComponentArray<ComponentT>>(m_componentArrays).getAll(entity); }
        template<typename ComponentT>
        ConstEntitySpecificRange<ComponentT> cgetAll(Entity entity) const { return std::get<ComponentArray<ComponentT>>(m_componentArrays).cgetAll(entity); }

        template<typename ComponentT>
        ComponentRange<ComponentT>& getAll()  { return std::get<ComponentArray<ComponentT>>(m_componentArrays); }
        template<typename ComponentT>
        ComponentRange<ComponentT> const& getAll() const { return std::get<ComponentArray<ComponentT>>(m_componentArrays); }
        template<typename ComponentT>
        ComponentRange<ComponentT> const& cgetAll() const { return std::get<ComponentArray<ComponentT>>(m_componentArrays); }

        template<typename ComponentT>
        bool has(Entity entity) const { return std::get<ComponentArray<ComponentT>>(m_componentArrays).has(entity); }

    private:
        std::tuple<ComponentArray<ComponentsT>...> m_componentArrays;
    };
}

#endif // !COMPONENT_POOL_HPP_INCLUDED