#ifndef REQUIREMENTS_HPP_INCLUDED
#define REQUIREMENTS_HPP_INCLUDED

#include "Parameters.hpp"

#include "utils/Typelist.hpp"

namespace ecs
{
    template<typename ... RequirementsT>
    struct Requirements{};

    struct Auto{};

    template<typename RequirementsT, typename ParametersTL>
    struct expand_requirements;

    template<typename RequirementsT, typename ParametersTL>
    using ExpandedRequirements = tl::to_typed_pack_t<Requirements, typename expand_requirements<RequirementsT, ParametersTL>::type>;
}

namespace ecs
{
    struct NoRequirement{};

    template<typename Parameter>
    struct parameter_to_requirement
    {
        using type = NoRequirement;
    };
    template<typename UnderlyingT>
    struct parameter_to_requirement<Component<UnderlyingT>>
    {
        using type = Component<UnderlyingT>;
    };

    template<typename RequirementT, typename ParametersTL>
    struct expand_one_requirement
    {
        using type = Typelist<RequirementT>;
    };

    template<typename ParametersTL>
    struct expand_one_requirement<Auto, ParametersTL>
    {
        using type = tl::remove_all_t<NoRequirement, tl::for_each_t<ParametersTL, parameter_to_requirement>>;
    };

    template<typename ... RequirementsT, typename ParametersTL>
    struct expand_requirements<Requirements<RequirementsT...>, ParametersTL>
    {
        using type = tl::remove_all_t<NoRequirement, tl::concat_all_t<typename expand_one_requirement<RequirementsT, ParametersTL>::type...>>;
    };
}

#endif // !REQUIREMENTS_HPP_INCLUDED
