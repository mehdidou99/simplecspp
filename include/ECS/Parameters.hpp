#ifndef PARAMETERS_HPP_INCLUDED
#define PARAMETERS_HPP_INCLUDED

#include "EntityPool.hpp"
#include "Component.hpp"
#include "ComponentRange.hpp"

#include "utils/Typelist.hpp"

namespace ecs
{
    template<typename ... ParametersT>
    struct Parameters{};

    using CompatibleEntities = EntityPool;

    template<typename ParametersTL> struct providable_parameters_impl;
    template<typename ParametersTL>
    using ProvidableParameters = tl::to_typed_pack_t<Parameters, typename providable_parameters_impl<ParametersTL>::type>;
}

namespace ecs
{
    struct ToRemove{};

    template<typename Parameter>
    struct tag_for_removal
    {
        using type = ToRemove;
    };
    template<typename UnderlyingT>
    struct tag_for_removal<Component<UnderlyingT>>
    {
        using type = Component<UnderlyingT>;
    };
    template<typename ComponentT>
    struct tag_for_removal<ComponentRange<ComponentT>>
    {
        using type = ComponentRange<ComponentT>;
    };
    template<>
    struct tag_for_removal<EntityPool>
    {
        using type = EntityPool;
    };

    template<typename ParametersTL>
    struct providable_parameters_impl
    {
        using type = tl::remove_all_t<ToRemove, tl::for_each_t<ParametersTL, tag_for_removal>>;
    };
}

#endif // !PARAMETERS_HPP_INCLUDED
