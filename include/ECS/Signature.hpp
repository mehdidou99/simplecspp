#ifndef SIGNATURE_HPP_INCLUDED
#define SIGNATURE_HPP_INCLUDED

#include <bitset>

#include "utils/Typelist.hpp"
#include "utils/utilities.hpp"

#include "ComponentRange.hpp"
#include "Component.hpp"
#include "ComponentPool.hpp"
#include "Requirements.hpp"

namespace ecs
{
    template<typename ... ComponentsT>
    struct Signature : std::bitset<sizeof...(ComponentsT)>{};

    template<typename ComponentPoolT>
    struct signature_type_impl;
    template<typename ComponentPoolT>
    using signature_type = typename signature_type_impl<ComponentPoolT>::type;

    using SignatureRepr = std::size_t;

    template<typename ComponentPoolT>
    struct RequirementsToSignatureConverter;

    template<typename SignatureT>
    struct SignatureChanger;

    template<typename ... ComponentsT>
    struct SignatureChanger<Signature<ComponentsT...>>
    {
        using SignatureType = Signature<ComponentsT...>;
        template<typename ComponentT>
        static void add(SignatureType& s);
        
        template<typename ComponentT>
        static void remove(SignatureType& s);
    };
}

namespace ecs
{
    template<typename ... ComponentsT>
    struct signature_type_impl<ComponentPool<ComponentsT...>>
    {
        using type = Signature<ComponentsT...>;
    };

    template<typename Requirement, typename ComponentsTL>
    struct one_requirement_signature_repr
    {
        static constexpr SignatureRepr value = 0;
    };
    template<typename UnderlyingT, typename ComponentsTL>
    struct one_requirement_signature_repr<Component<UnderlyingT>, ComponentsTL>
    {
        static constexpr SignatureRepr value = utils::pow2(tl::index_of_v<ComponentsTL, Component<UnderlyingT>>);
    };
    template<typename Requirement, typename ComponentsTL>
    constexpr auto one_requirement_signature_repr_v = one_requirement_signature_repr<Requirement, ComponentsTL>::value;

    template<typename ... ComponentsT>
    struct RequirementsToSignatureConverterImpl
    {
        template<typename ... Requirements>
        static constexpr SignatureRepr convertToSigRepr = (one_requirement_signature_repr_v<Requirements, Typelist<ComponentsT...>> + ...) ;

        template<typename ... Requirements>
        static constexpr Signature<ComponentsT...> convert { convertToSigRepr<Requirements...> };
    };

    template<typename ... ComponentsT>
    struct RequirementsToSignatureConverter<ComponentPool<ComponentsT...>> : RequirementsToSignatureConverterImpl<ComponentsT...> {};

    template<typename ... ComponentsT>
    template<typename ComponentT>
    void SignatureChanger<Signature<ComponentsT...>>::add(SignatureType& s)
    {
        constexpr auto idx = tl::index_of_v<Typelist<ComponentsT...>, ComponentT>;
        s[idx] = true;
    }

    template<typename ... ComponentsT>
    template<typename ComponentT>
    void SignatureChanger<Signature<ComponentsT...>>::remove(SignatureType& s)
    {
        constexpr auto idx = tl::index_of_v<Typelist<ComponentsT...>, ComponentT>;
        s[idx] = false;
    }
}

#endif // !SIGNATURE_HPP_INCLUDED
