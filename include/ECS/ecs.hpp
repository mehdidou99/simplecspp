#ifndef ECS_HPP_INCLUDED
#define ECS_HPP_INCLUDED

#include "ComponentPool.hpp"
#include "SystemPool.hpp"
#include "EntitySignaturesPool.hpp"
#include "EntityFactory.hpp"
#include "utils/EventSender.hpp"

#include <type_traits>

namespace ecs
{
    template<typename ... ComponentsT>
    struct Components{};

    template<typename ... SystemsT>
    struct Systems{};

    template<typename ComponentListT, typename SystemListT>
    class ecs;

    template<typename ... ComponentsT, typename ... SystemsT>
    class ecs<Components<ComponentsT...>, Systems<SystemsT...>>
    {
    private:
        using ComponentPoolType = ComponentPool<ComponentsT...>;
        using SystemPoolType = SystemPool<ComponentPoolType, SystemsT...>;
        using EntitySignaturesPoolType = EntitySignaturesPool<ComponentsT...>;
    public:
        template<typename ... Args>
        ecs(Args&&... systemArgs);

        Entity createEntity();

        template<typename ComponentT, typename ... Args>
        ComponentT& add(Entity entity, Args&& ... args);

        template<typename ComponentT>
        void remove(Entity entity);

        void destroy(Entity entity);

        void deactivate(Entity entity);
        void activate(Entity entity);

        template<typename SystemT, typename ... Args>
        auto update(Args&& ... args);

        template<typename SystemT>
        SystemT& getSystem();

        template<typename SystemT>
        SystemT const& getSystem() const;

        template<typename ComponentT>
        bool has(Entity entity) { return m_components.template has<ComponentT>(entity); }

        template<typename ComponentT>
        ComponentT& get(Entity entity) { return m_components.template get<ComponentT>(entity); }

        template<typename ComponentT>
        ComponentT const& get(Entity entity) const { return m_components.template get<ComponentT>(entity); }

        template<typename Callback>
        bool subscribeToEntityDestroyedEvent(std::string const& name, Callback&& callback) { return m_entityDestroyedEvent.subscribe(name, std::forward<Callback>(callback)); }

        template<typename Callback>
        bool unsubscribeToEntityDestroyedEvent(std::string const& name, Callback&& callback) { return m_entityDestroyedEvent.unsubscribe(name, std::forward<Callback>(callback)); }

    public:
        static std::size_t initialReservedSize;

    private:
        ComponentPoolType m_components;
        SystemPoolType m_systems;
        EntitySignaturesPoolType m_signatures;
        EntityFactory m_entityFactory;

        EventSender<Entity, EventPassingTraits::ByValue> m_entityDestroyedEvent;
    };

    template<typename ComponentListT, typename ... SystemsT>
    ecs<ComponentListT, Systems<std::remove_reference_t<SystemsT>>...> create(SystemsT&&... systems);
}

namespace ecs
{
    template<typename ... ComponentsT, typename ... SystemsT>
    std::size_t ecs<Components<ComponentsT...>, Systems<SystemsT...>>::initialReservedSize = defaults::initialComponentArraySize;

    template<typename ... ComponentsT, typename ... SystemsT>
    template<typename ... Args>
    ecs<Components<ComponentsT...>, Systems<SystemsT...>>::ecs(Args&&... systemArgs)
    : m_components{ initialReservedSize }
    , m_systems { m_components, std::forward<Args>(systemArgs)... }
    , m_signatures {}
    , m_entityFactory {}
    , m_entityDestroyedEvent {}
    {}

    template<typename ... ComponentsT, typename ... SystemsT>
    Entity ecs<Components<ComponentsT...>, Systems<SystemsT...>>::createEntity()
    {
        auto e = m_entityFactory.create();
        m_signatures.onEntityCreated(e);
        return e;
    }

    template<typename ... ComponentsT, typename ... SystemsT>
    template<typename ComponentT, typename ... Args>
    ComponentT& ecs<Components<ComponentsT...>, Systems<SystemsT...>>::add(Entity entity, Args&&... args)
    {
        auto signature = m_signatures.template onEntityComponentAdded<ComponentT>(entity);
        ComponentT& res = m_components.template add<ComponentT>(entity, std::forward<Args>(args)...);
        m_systems.onEntitySignatureChanged(entity, signature);

        return res;
    }

    template<typename ... ComponentsT, typename ... SystemsT>
    template<typename ComponentT>
    void ecs<Components<ComponentsT...>, Systems<SystemsT...>>::remove(Entity entity)
    {
        auto signature = m_signatures.onEntityComponentRemoved(entity);
        m_components.template remove<ComponentT>(entity);
        m_systems.onEntitySignatureChanged(entity, signature);
    }

    template<typename ... ComponentsT, typename ... SystemsT>
    void ecs<Components<ComponentsT...>, Systems<SystemsT...>>::destroy(Entity entity)
    {
        m_signatures.onEntityDestroyed(entity);
        m_components.onEntityDestroyed(entity);
        m_systems.onEntityDestroyed(entity);
        m_entityFactory.onEntityDestroyed(entity);
        
        m_entityDestroyedEvent.send(entity);
    }

    template<typename ... ComponentsT, typename ... SystemsT>
    void ecs<Components<ComponentsT...>, Systems<SystemsT...>>::deactivate(Entity entity)
    {
        m_systems.onEntityDestroyed(entity);
        
        m_entityDestroyedEvent.send(entity);
    }

    template<typename ... ComponentsT, typename ... SystemsT>
    void ecs<Components<ComponentsT...>, Systems<SystemsT...>>::activate(Entity entity)
    {
        m_systems.onEntitySignatureChanged(entity, m_signatures.getSignature(entity));
        
        m_entityDestroyedEvent.send(entity);
    }

    template<typename ... ComponentsT, typename ... SystemsT>
    template<typename SystemT, typename ... Args>
    auto ecs<Components<ComponentsT...>, Systems<SystemsT...>>::update(Args&& ... args)
    {
        return m_systems.template update<SystemT>(std::forward<Args>(args)...);
    }

    template<typename ... ComponentsT, typename ... SystemsT>
    template<typename SystemT>
    SystemT& ecs<Components<ComponentsT...>, Systems<SystemsT...>>::getSystem()
    {
        return m_systems.template get<SystemT>();
    }

    template<typename ... ComponentsT, typename ... SystemsT>
    template<typename SystemT>
    SystemT const& ecs<Components<ComponentsT...>, Systems<SystemsT...>>::getSystem() const
    {
        return m_systems.template get<SystemT>();
    }
}

#endif // !ECS_HPP_INCLUDED