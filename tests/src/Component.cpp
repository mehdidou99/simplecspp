#include "gtest/gtest.h"

#include "ECS/Component.hpp"

#include "DumbComponent.hpp"

namespace
{

class ComponentTest : public ::testing::Test
{
    protected:
        ComponentTest()
        : d1 { 3, 5 }
        {}

    protected:
        DumbComponent d1;
};

TEST_F(ComponentTest, EntityArgsConstructorWorks)
{
    EXPECT_EQ(d1.entity(), 3);
    EXPECT_EQ(d1.i, 5);
}

TEST_F(ComponentTest, ComparisonOperatorsWork)
{
    DumbComponent dEqual { d1.entity(), d1.i };
    DumbComponent dGreaterUnderlying { d1.entity(), d1.i+1 };
    DumbComponent dDifferentEntity { d1.entity()+1, d1.i };
    DumbComponent dGreaterUnderlyingDifferentEntity { d1.entity()+1, d1.i+1 };
    EXPECT_EQ(d1, dEqual);
    EXPECT_EQ(d1, dDifferentEntity);
    EXPECT_FALSE(d1 == dGreaterUnderlying);
    EXPECT_FALSE(d1 == dGreaterUnderlyingDifferentEntity);
    EXPECT_NE(d1, dGreaterUnderlying);
    EXPECT_NE(d1, dGreaterUnderlyingDifferentEntity);
    EXPECT_FALSE(d1 != dEqual);
    EXPECT_FALSE(d1 != dDifferentEntity);

    EXPECT_LT(d1, dGreaterUnderlying);
    EXPECT_LT(d1, dGreaterUnderlyingDifferentEntity);
    EXPECT_FALSE(dGreaterUnderlying < d1);
    EXPECT_FALSE(dGreaterUnderlyingDifferentEntity < d1);
    EXPECT_FALSE(d1 < dEqual);
    EXPECT_FALSE(dEqual < d1);
    EXPECT_FALSE(d1 < dDifferentEntity);
    EXPECT_FALSE(dDifferentEntity < d1);

    EXPECT_GT(dGreaterUnderlying, d1);
    EXPECT_GT(dGreaterUnderlyingDifferentEntity, d1);
    EXPECT_FALSE(d1 > dGreaterUnderlying);
    EXPECT_FALSE(d1 > dGreaterUnderlyingDifferentEntity);
    EXPECT_FALSE(d1 > dEqual);
    EXPECT_FALSE(dEqual > d1);
    EXPECT_FALSE(d1 > dDifferentEntity);
    EXPECT_FALSE(dDifferentEntity > d1);

    EXPECT_LE(d1, dGreaterUnderlying);
    EXPECT_LE(d1, dGreaterUnderlyingDifferentEntity);
    EXPECT_FALSE(dGreaterUnderlying <= d1);
    EXPECT_FALSE(dGreaterUnderlyingDifferentEntity <= d1);
    EXPECT_LE(d1, dEqual);
    EXPECT_LE(dEqual, d1);
    EXPECT_LE(d1, dDifferentEntity);
    EXPECT_LE(dDifferentEntity, d1);

    EXPECT_GE(dGreaterUnderlying, d1);
    EXPECT_GE(dGreaterUnderlyingDifferentEntity, d1);
    EXPECT_FALSE(d1 >= dGreaterUnderlying);
    EXPECT_FALSE(d1 >= dGreaterUnderlyingDifferentEntity);
    EXPECT_GE(d1, dEqual);
    EXPECT_GE(dEqual, d1);
    EXPECT_GE(d1, dDifferentEntity);
    EXPECT_GE(dDifferentEntity, d1);
}

/* TODO : Write comparison tests between DumbComponent and Dumb */

TEST_F(ComponentTest, CopyConstructorWorks)
{
    DumbComponent copy { d1 };
    EXPECT_EQ(d1.entity(), copy.entity());
    EXPECT_EQ(d1, copy);
}

TEST_F(ComponentTest, EntityOtherComponentConstructorWorks)
{
    DumbComponent d2 { d1.entity() + 1, d1 };
    EXPECT_EQ(d2.entity(), d1.entity() + 1);
    EXPECT_EQ(d2, d1);
}

TEST_F(ComponentTest, CopyAssignmentWorks)
{
    DumbComponent d2 { d1.entity() + 1, d1.i + 1 };
    d2 = d1;
    EXPECT_EQ(d2.entity(), d1.entity() + 1);
    EXPECT_EQ(d2, d1);
}

TEST_F(ComponentTest, WholeCopyAssignmentWorks)
{
    DumbComponent d2 { d1.entity() + 1, d1.i + 1 };
    d2.assignAsWhole(d1);
    EXPECT_EQ(d2.entity(), d1.entity());
    EXPECT_EQ(d2, d1);
}

TEST_F(ComponentTest, MoveConstructorWorks)
{
    DumbComponent d1Copy { d1 };
    DumbComponent d2 { std::move(d1) };
    EXPECT_EQ(d2.entity(), d1Copy.entity());
    EXPECT_EQ(d2, d1Copy);
    EXPECT_EQ(d1.i, DumbComponent::movedAwayWithConstruction);
}

TEST_F(ComponentTest, EntityOtherMoveableComponentConstructorWorks)
{
    DumbComponent d1Copy { d1 };
    DumbComponent d2 { d1.entity() + 1, std::move(d1) };
    EXPECT_EQ(d2.entity(), d1Copy.entity() + 1);
    EXPECT_EQ(d2, d1Copy);
    EXPECT_EQ(d1.i, DumbComponent::movedAwayWithConstruction);
}

TEST_F(ComponentTest, MoveAssignment)
{
    DumbComponent d1Copy { d1 };
    DumbComponent d2 { d1.entity() + 1, d1.i + 1 };
    d2 = std::move(d1);
    EXPECT_EQ(d2.entity(), d1Copy.entity() + 1);
    EXPECT_EQ(d2, d1Copy);
    EXPECT_EQ(d1.i, DumbComponent::movedAwayWithAssignment);
}

TEST_F(ComponentTest, WholeMoveAssignment)
{
    DumbComponent d1Copy { d1 };
    DumbComponent d2 { d1.entity() + 1, d1.i + 1 };
    d2.assignAsWhole(std::move(d1));
    EXPECT_EQ(d2.entity(), d1Copy.entity());
    EXPECT_EQ(d2, d1Copy);
    EXPECT_EQ(d1.i, DumbComponent::movedAwayWithAssignment);
}

}


