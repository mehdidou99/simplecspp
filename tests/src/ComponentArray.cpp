#include <algorithm>
#include <deque>
#include <iostream>

#include "gtest/gtest.h"

#include "ECS/ComponentArray.hpp"

#include "DumbComponent.hpp"

namespace
{

using Values = std::pair<ecs::Entity, int>;
using ValuesVector = std::vector<Values>;

struct DumbComponentAgainstValuesComparator
{
    bool operator()(DumbComponent const& c, Values const& v)
    {
        return c.entity() == v.first && c.i == v.second;
    }

    bool operator()(Values const& v, DumbComponent const& c)
    {
        return c.entity() == v.first && c.i == v.second;
    }

    bool operator()(DumbComponent const& c1, DumbComponent const& c2)
    {
        return c1.entity() == c2.entity() && c1.i == c2.i;
    }

    bool operator()(Values const& v1, Values const& v2)
    {
        return v1.first == v2.first && v1.second == v2.second;
    }
};

using TestedArray = ecs::ComponentArray<DumbComponent>;

class ComponentArrayTest : public ::testing::Test
{
    protected:
        ComponentArrayTest()
        : empty{}
        , components{}
        , d { 3, 5 }
        {
            for (auto i = 0; i < 7; ++i)
                components.add(i%3+1, 11+i);
        }

    protected:
        TestedArray empty;
        TestedArray components;
        DumbComponent d;
};

TEST_F(ComponentArrayTest, MethodAddAddsComponent)
{
    DumbComponent&c { empty.add(d.entity(), d.i) };
    EXPECT_EQ(c.entity(), d.entity());
    EXPECT_EQ(c, d);
    ASSERT_EQ(std::size(empty), static_cast<std::size_t>(1));
    EXPECT_EQ(std::begin(empty)->entity(), d.entity());
    EXPECT_EQ(std::begin(empty)->i, d.i);
}

TEST_F(ComponentArrayTest, MethodAddCanAddComponentByCopying)
{
    DumbComponent&c { empty.add(d) };
    EXPECT_EQ(c.entity(), d.entity());
    EXPECT_EQ(c, d);
    ASSERT_EQ(std::size(empty), static_cast<std::size_t>(1));
    EXPECT_EQ(std::begin(empty)->entity(), d.entity());
    EXPECT_EQ(std::begin(empty)->i, d.i);
}

TEST_F(ComponentArrayTest, MethodAddCanAddComponentByMoving)
{
    DumbComponent dCopy { d };
    DumbComponent&c { empty.add(std::move(d)) };
    EXPECT_EQ(c.entity(), dCopy.entity());
    EXPECT_EQ(c, dCopy);
    EXPECT_EQ(std::size(empty), static_cast<std::size_t>(1));
    EXPECT_EQ(std::begin(empty)->entity(), dCopy.entity());
    EXPECT_EQ(std::begin(empty)->i, dCopy.i);
    EXPECT_EQ(d.i, DumbComponent::movedAwayWithConstruction);
}

TEST_F(ComponentArrayTest, GetGetsOneOfEntityComponents)
{
    const ecs::Entity entityToGet { 2 };
    DumbComponent& c { components.get(entityToGet) };
    EXPECT_EQ(c.entity(), entityToGet);
    EXPECT_TRUE(std::any_of(std::begin(components), std::end(components), [&c](DumbComponent const& component)
                    {
                        return component.entity() == c.entity() && component == c;
                    }));
}

TEST_F(ComponentArrayTest, ConstantGetGetsOneOfEntityComponents)
{
    const ecs::Entity entityToGet { 2 };
    DumbComponent const& c { static_cast<TestedArray const&>(components).get(entityToGet) };
    EXPECT_EQ(c.entity(), entityToGet);
    EXPECT_TRUE(std::any_of(std::begin(components), std::end(components), [&c](DumbComponent const& component)
                    {
                        return component.entity() == c.entity() && component == c;
                    }));
}

TEST_F(ComponentArrayTest, GetAllGetsCorrectEntitySpecificRange)
{
    const ecs::Entity entityToGet { 1 };
    ecs::EntitySpecificRange<DumbComponent> entityComponents { components.getAll(entityToGet) };
    ValuesVector goodContent
        {
            { 1, 11 },
            { 1, 14 },
            { 1, 17 },
        };
    ASSERT_TRUE(std::is_permutation(std::begin(entityComponents), std::end(entityComponents), std::begin(goodContent), DumbComponentAgainstValuesComparator{}));
}

TEST_F(ComponentArrayTest, ConstantGetAllGetsCorrectConstantEntitySpecificRange)
{
    const ecs::Entity entityToGet { 1 };
    ecs::ConstEntitySpecificRange<DumbComponent> entityComponents { static_cast<TestedArray const&>(components).getAll(entityToGet) };
    ValuesVector goodContent
        {
            { 1, 11 },
            { 1, 14 },
            { 1, 17 },
        };
    ASSERT_TRUE(std::is_permutation(std::begin(entityComponents), std::end(entityComponents), std::begin(goodContent), DumbComponentAgainstValuesComparator{}));
}

TEST_F(ComponentArrayTest, CGetAllGetsCorrectConstantEntitySpecificRange)
{
    const ecs::Entity entityToGet { 1 };
    ecs::ConstEntitySpecificRange<DumbComponent> entityComponents { components.cgetAll(entityToGet) };
    ValuesVector goodContent
        {
            { 1, 11 },
            { 1, 14 },
            { 1, 17 },
        };
    ASSERT_TRUE(std::is_permutation(std::begin(entityComponents), std::end(entityComponents), std::begin(goodContent), DumbComponentAgainstValuesComparator{}));
}

TEST_F(ComponentArrayTest, CopyConstructorWorks)
{
    TestedArray componentsCopy { components };
    ASSERT_EQ(std::size(componentsCopy), std::size(components));
    for (std::size_t i = 0; i < std::size(componentsCopy); ++i)
    {
        EXPECT_EQ(componentsCopy[i].entity(), components[i].entity());
        EXPECT_EQ(componentsCopy[i], components[i]);
    }
}

TEST_F(ComponentArrayTest, CopyAssignmentWorks)
{
    TestedArray componentsCopy{};
    componentsCopy.add(3, 5);
    componentsCopy = components;
    ASSERT_EQ(std::size(componentsCopy), std::size(components));
    for (std::size_t i = 0; i < std::size(componentsCopy); ++i)
    {
        EXPECT_EQ(componentsCopy[i].entity(), components[i].entity()) << i;
        EXPECT_EQ(componentsCopy[i], components[i]);
    }
}

TEST_F(ComponentArrayTest, MoveConstructorWorks)
{
    TestedArray componentsCopy { components };
    TestedArray componentsMoved { std::move(components) };
    ASSERT_EQ(std::size(componentsMoved), std::size(componentsCopy));
    for (std::size_t i = 0; i < std::size(componentsMoved); ++i)
    {
        EXPECT_EQ(componentsMoved[i].entity(), componentsCopy[i].entity());
        EXPECT_EQ(componentsMoved[i], componentsCopy[i]);
    }
}

TEST_F(ComponentArrayTest, MoveAssignmentWorks)
{
    TestedArray componentsCopy{ components };
    TestedArray componentsMoved{};
    componentsMoved.add(3, 5);
    componentsMoved = std::move(components);
    ASSERT_EQ(std::size(componentsMoved), std::size(componentsCopy));
    for (std::size_t i = 0; i < std::size(componentsCopy); ++i)
    {
        EXPECT_EQ(componentsMoved[i].entity(), componentsCopy[i].entity());
        EXPECT_EQ(componentsMoved[i], componentsCopy[i]);
    }
}

TEST_F(ComponentArrayTest, RemoveComponentsRemovesAllEntityComponents)
{
    std::cout << std::endl;
    constexpr ecs::Entity entityToRemove { 1 };
    components.removeComponents(entityToRemove);
    ValuesVector goodValues
        {
            { 3, 16 },
            { 2, 12 },
            { 3, 13 },
            { 2, 15 }
        };
    ASSERT_EQ(std::size(components), std::size(goodValues));
        
    for(std::size_t i = 0; i < std::size(components); ++i)
    {
        EXPECT_EQ(components[i].entity(), goodValues[i].first);
        EXPECT_EQ(components[i].i, goodValues[i].second);
    }
}

TEST_F(ComponentArrayTest, RemoveComponentsDoesNotInvalidateAccessMaps)
{
    constexpr ecs::Entity entityToRemove { 1 };
    components.removeComponents(entityToRemove);
    constexpr ecs::Entity entityToGet { 2 };
    auto const& component = components.get(entityToGet);
    EXPECT_EQ(component.entity(), entityToGet);
    EXPECT_TRUE(std::any_of(std::begin(components), std::end(components), [&component](auto const& c)
                    {
                        return component.entity() == c.entity() && component == c;
                    }));
}

TEST_F(ComponentArrayTest, RemoveComponentsEmptiesArrayWhenOnlyOneEntityPresent)
{
    TestedArray oneEntityComponentArray{};
    constexpr ecs::Entity entityToRemove { 1 };
    for (auto i = 0; i < 4; ++i)
            oneEntityComponentArray.add(entityToRemove, i-1);
    oneEntityComponentArray.removeComponents(entityToRemove);
    EXPECT_TRUE(oneEntityComponentArray.empty());
}

TEST_F(ComponentArrayTest, OnEntityDestroyedRemovesComponentsWhenEntityPresent)
{
    constexpr ecs::Entity entityToRemove { 1 };
    components.onEntityDestroyed(entityToRemove);
    ValuesVector goodValues
        {
            { 3, 16 },
            { 2, 12 },
            { 3, 13 },
            { 2, 15 }
        };
    ASSERT_EQ(std::size(components), std::size(goodValues));
    for(std::size_t i = 0; i < std::size(components); ++i)
    {
        EXPECT_EQ(components[i].entity(), goodValues[i].first);
        EXPECT_EQ(components[i].i, goodValues[i].second);
    }
}

TEST_F(ComponentArrayTest, OnEntityDestroyedDoesNothingWhenEntityMissing)
{
    TestedArray componentsCopy { components };
    constexpr ecs::Entity entityToRemove { 4 };
    componentsCopy.onEntityDestroyed(entityToRemove);
    ASSERT_EQ(std::size(components), std::size(componentsCopy));
    for(std::size_t i = 0; i < std::size(components); ++i)
    {
        EXPECT_EQ(components[i].entity(), componentsCopy[i].entity());
        EXPECT_EQ(components[i].i, componentsCopy[i].i);
    }
}
}
