#include "gtest/gtest.h"

#include "ECS/EntityFactory.hpp"

namespace
{

class EntityFactoryTest : public ::testing::Test
{
    protected:
        EntityFactoryTest()
        : factory{}
        {}

    protected:
        ecs::EntityFactory factory;
};

TEST_F(EntityFactoryTest, CreateEntityWorksInitially)
{
    EXPECT_EQ(factory.create(), 0);
    EXPECT_EQ(factory.create(), 1);
    EXPECT_EQ(factory.create(), 2);
    EXPECT_EQ(factory.create(), 3);
    EXPECT_EQ(factory.create(), 4);
}

TEST_F(EntityFactoryTest, DestroyEntityWorks)
{
    factory.create();
    factory.create();
    factory.create();
    factory.onEntityDestroyed(2);
    EXPECT_EQ(factory.create(), 2);
    factory.onEntityDestroyed(1);
    factory.onEntityDestroyed(2);
    EXPECT_EQ(factory.create(), 1);
    EXPECT_EQ(factory.create(), 2);
    EXPECT_EQ(factory.create(), 3);
}

}
