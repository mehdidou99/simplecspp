#ifndef DUMB_COMPONENT_HPP_INCLUDED
#define DUMB_COMPONENT_HPP_INCLUDED

#include <iostream>

#include "ECS/Component.hpp"

struct Dumb
{
    static constexpr int movedAwayWithConstruction { std::numeric_limits<int>::min() };
    static constexpr int movedAwayWithAssignment { std::numeric_limits<int>::max() };

    Dumb() = default;
    
    Dumb(int i_)
    : i { i_ }
    {}
    
    Dumb(Dumb const& rhs) = default;
    Dumb& operator=(Dumb const& rhs) = default;
    
    Dumb(Dumb&& rhs)
    : i { std::move(rhs.i) }
    {
        rhs.i = movedAwayWithConstruction;
    }
    
    Dumb& operator=(Dumb&& rhs)
    {
        i = std::move(rhs.i);
        rhs.i = movedAwayWithAssignment;
        return *this;
    }
        
    int i { 0 };
};

inline bool operator ==(Dumb const& a, Dumb const&b) { return a.i == b.i; }
inline bool operator !=(Dumb const& a, Dumb const& b) { return !(a == b); }
inline bool operator <(Dumb const& a, Dumb const& b) { return a.i < b.i; }
inline bool operator >(Dumb const& a, Dumb const& b) { return a.i > b.i; }
inline bool operator <=(Dumb const& a, Dumb const& b) { return !(a > b); }
inline bool operator >=(Dumb const& a, Dumb const& b) { return !(a < b); }

using DumbComponent = ecs::Component<Dumb>;

inline void print(DumbComponent const& d)
{
    std::cout << "Entity : " << d.entity() << " - Value : " << d.i << std::endl;
}

inline bool correctValues(DumbComponent const& c, ecs::Entity e, int i)
{
    return c.entity() == e && c.i == i;
}

#endif // !DUMB_COMPONENT_HPP_INCLUDED