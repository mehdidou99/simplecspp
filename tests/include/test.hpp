#ifndef TEST_HPP_INCLUDED
#define TEST_HPP_INCLUDED

#include <string>
#include <iostream>
#include <cassert>
#include <exception>

struct TestFailed : public std::runtime_error
{
    explicit TestFailed(std::string const& what_arg)
    : runtime_error(what_arg)
    {}
};

class Test
{
public:
    Test(std::string const& name)
    : m_name { name }
    , m_failed { false }
    {
        std::cout << "BEGINNING TESTS ON " << name << '.' << std::endl;
    }

    void endTests()
    {
        if (m_failed) 
        {
            std::cerr << "TESTS ON " << m_name << " FAILED." << std::endl;
            throw TestFailed{ std::string { "TEST FAILED" } + m_name };
        }
        else std::cout << "TESTS ON " << m_name << " PASSED." << std::endl;        
    }

    template<typename TestT>
    void testCase(std::string const& testName, TestT test)
    {
        if(!test())
        {
            std::cerr << "TEST FAILED : " << testName << '.' << std::endl;
            m_failed = true;
        }
        else
            std::cout << "TEST PASSED : " << testName << '.' << std::endl;
    }

private:
    std::string m_name;
    bool m_failed;
};

#define BEGIN_TESTS(name) int main() { Test TEST_INSTANCE { name };
#define END_TESTS() TEST_INSTANCE.endTests(); return 0; }

#define TEST_CASE(name) TEST_INSTANCE.testCase(name, [&]() -> bool {
#define END_TEST_CASE() });

#endif // !TEST_HPP_INCLUDED